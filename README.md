## Data Preparation

This folder includes

1. **DataAugmentation** performs three augmentations on the given dataset.
2. **MFCC** includes methods to generate MFCCs and divide them to training/test sets in addition to dividing images into training/test folders.
3. **ML_toSpectro** transforms waveforms into spectrograms and mel-spectrogams and saves them to the drive.

---

## Models

1. **ML_KNN** runs a grid search on a KNN classifier using augmented data (after generating MFCCs and normalization).
2. **ML_lstm** evaluates a RNN LSTM model with non-augmented data using MFCCs (after grid search) and then refits the model and evaluate it on augmented data.
3. **ML_resnet** creates a ResNet50 model with different combinations of number of frozen layers, choice of optimizer (SGD or Adam) and features (spectrogram or mel-spectrogram).

---

## Data

### Kaggle Dataset

https://www.kaggle.com/c/rfcx-species-audio-detection

### Important Links

This folder includes links to drive containing the original audio recordings, recordings after augmentation, spectrogram images and mel-spetrogram images.

### train_tp.csv

This file contains data about the recordings (identified species, t_min, t_max, etc.).